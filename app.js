let inputEle = document.querySelector('#inputBox');
let addbtnEle = document.querySelector('#add');
let todoListEle = document.querySelector('#todo-list');

let arr = [];
if (localStorage.length == 0) {
  localStorage.setItem('toDoItems', '[]');
} else {
  arr = [...JSON.parse(localStorage['toDoItems'])];
}
console.log(arr);
// Function to create a new list and add it
function createNewList(text, id, status) {
  let neWlist = document.createElement('li');
  neWlist.setAttribute('class', 'todoList');
  neWlist.setAttribute('id', id);

  let checkbox = document.createElement('input');
  checkbox.setAttribute('type', 'checkbox');
  checkbox.setAttribute('class', 'checkbox');

  //checkbox event handling on page loading
  if (status) {
    checkbox.checked = true;
    neWlist.setAttribute('class', 'strike');
  } else {
    checkbox.checked = false;
    neWlist.setAttribute('class', 'remove-strike');
  }
  // create edit button
  let editBtn = document.createElement('button');
  editBtn.setAttribute('class', 'editButton');
  editBtn.appendChild(document.createTextNode('Edit'));

  // create delete button
  let deleteBtn = document.createElement('button');
  deleteBtn.setAttribute('class', 'deleteButton');
  deleteBtn.appendChild(document.createTextNode('Delete'));

  //append in new list
  neWlist.appendChild(checkbox);
  neWlist.appendChild(document.createTextNode(text));
  neWlist.appendChild(deleteBtn);
  neWlist.appendChild(editBtn);
  //append in todo list
  todoListEle.appendChild(neWlist);
  return neWlist;
}

//Function to add list after pressing add button
function addElementWithButton(index) {
  addbtnEle.addEventListener('click', function () {
    let text = inputEle.value;
    if (text) {
      let randomId = Math.floor(Math.random() * 1000) + text.slice(0, 3);
      obj = { id: randomId, text: text, status: false };
      arr.push(obj);
      localStorage.setItem('toDoItems', JSON.stringify(arr));
      let element = createNewList(text, randomId, false);
      todoListEle.appendChild(element);
      inputEle.value = '';
    }
  });
}

//Function to add list after hit enter button
let count = 0;
function addListWithEnter(index) {
  inputEle.addEventListener('keypress', (event) => {
    if (event.keyCode == 13 && inputEle.value) {
      let text = inputEle.value;

      //code to push the elemet in Local storage

      let randomId = Math.floor(Math.random() * 1000) + text.slice(0, 3);
      let element = createNewList(text, randomId, false);
      obj = { id: randomId, text: text, status: false };
      arr.push(obj);
      localStorage.setItem('toDoItems', JSON.stringify(arr));
      todoListEle.appendChild(element);
      inputEle.value = '';
    }
  });
}

function checkBox() {
  todoListEle.addEventListener('change', (event) => {
    let target = event.target;
    if (target.parentNode.classList != 'strike') {
      target.parentNode.classList = 'strike';
      for (let index = 0; index < arr.length; index++) {}
    } else {
      target.parentNode.classList = 'remove-strike';
    }

    for (let index = 0; index < arr.length; index++) {
      if (target.parentNode.id === arr[index]['id']) {
        if (target.parentNode.classList.value === 'strike') {
          arr[index]['status'] = true;
        } else {
          arr[index]['status'] = false;
        }
        break;
        //console.log(target.parentNode.id, arr[index]['id']);
      }
    }
    localStorage.setItem('toDoItems', JSON.stringify(arr));
  });
}

//Function to delete todoList
function deleteTodoList() {
  todoListEle.addEventListener('click', (event) => {
    let target = event.target;
    let list = target.parentNode;
    if (target.classList.value === 'deleteButton') {
      list.remove();
      for (let index = 0; index < arr.length; index++) {
        if (list.id === arr[index]['id']) {
          arr.splice(index, 1);
          break;
        }
      }
      localStorage.setItem('toDoItems', JSON.stringify(arr));
    }
  });
}

// Function to edit todo list
function editTodoList() {
  todoListEle.addEventListener('click', (event) => {
    let target = event.target;
    let list = target.parentNode;
    if (target.classList.value === 'editButton') {
      list.remove();
      for (let index = 0; index < arr.length; index++) {
        if (list.id === arr[index]['id']) {
          inputEle.value = arr[index].text;
          arr.splice(index, 1);
          break;
        }
      }
      localStorage.setItem('toDoItems', JSON.stringify(arr));
    }
  });
}

function onPageLoad() {
  for (let index = 0; index < arr.length; index++) {
    createNewList(arr[index]['text'], arr[index]['id'], arr[index]['status']);
  }
}

addElementWithButton();
addListWithEnter('tata');
checkBox();
deleteTodoList();
editTodoList();
onPageLoad();
